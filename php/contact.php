<?php 

//include_once('inc/class.simple_mail.php');
require 'php-mailer/PHPMailerAutoload.php';
include_once('inc/gump.class.php');
include_once('Mailchimp.php');
include_once('mail-config.php');

// Check Data
$isValid = GUMP::is_valid($_POST, array(
	'nombre' => 'required',
	'email' => 'required|valid_email',
	'telefono' => 'required|numeric|min_len,10'
	));

if($isValid === true) {
	
	$mail = new PHPMailer;
	//$mail->SMTPDebug = 2; // Enable verbose debug output
	$mail->isSMTP(); // Set mailer to use SMTP
	$mail->Host = 'mail.megridigital.com';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true; // Enable SMTP authentication
	$mail->Username = 'cesar@megridigital.com'; // SMTP username
	$mail->Password = 'M36r1d'; // SMTP password
	//$mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
	//$mail->Port = 587; // TCP port to connect to
	
	$mail->setLanguage('en');
	$mail->CharSet = 'utf-8';
	
	$mail->setFrom('cesar@megridigital.com', 'Megri Digital');
	$mail->addAddress(YOUR_EMAIL_ADDRESS,YOUR_COMPANY_NAME);
	$mail->addReplyTo(htmlspecialchars($_POST['email']), htmlspecialchars($_POST['nombre']));
	//$mail->addCC();
	//$mail->addBCC('cesar@megridigital.com','Cesar Arciniega');
	
	//$mail->addAttachment('/var/tmp/file.tar.gz'); // Add attachments
	//$mail->addAttachment('/tmp/image.jpg', 'new.jpg'); // Optional name
	$mail->isHTML(true); // Set email format to HTML
	
	$mail->Subject = 'Nuevo Contacto - Nezt';
	$mail->Body    = createMessage();
	//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	// Submit Lead
	//$Mailchimp = new Mailchimp(MAILCHIMP_API_KEY);
	//$Mailchimp_Lists = new Mailchimp_Lists( $Mailchimp );
	//$subscriber = $Mailchimp_Lists->subscribe( MAILCHIMP_LIST_ID, array('email' => htmlentities($_POST['email'])));

	if(!$mail->send()) {
		$result = array(
		'result' => 'error', 
		'msg' => array('<span class="error">Error: '.$mail->ErrorInfo.'</span>')
		);

		echo json_encode($result);
	} else {
		$result = array(
		'result' => 'success', 
		'msg' => array('<span class="success">¡Gracias! Revisa tu correo para confirmar tu suscripción.</span><script>ga("send", "event", "lead", "form", "success");</script>')
		);

		echo json_encode($result);
	}
} else {
	$result = array(
		'result' => 'error', 
		'msg' => $isValid
		);

	echo json_encode($result);
}


function createMessage()
{
	$body  = "Has recibido un nuevo formulario de contacto: <br><br>";
	$body .=	"<strong>Nombre:</strong>  ".htmlspecialchars($_POST['nombre'])." <br><br>";
	$body .=	"<strong>Email:</strong>  ".htmlspecialchars($_POST['email'])." <br><br>";
	$body .=	"<strong>Telefono:</strong>  ".htmlspecialchars($_POST['telefono'])." <br><br>";

	return $body;
}
