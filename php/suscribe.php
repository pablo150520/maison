<?php 

include_once('inc/gump.class.php');

include_once('mail-config.php');
include_once('Mailchimp.php');

// Check Data
$isValid = GUMP::is_valid($_POST, array(
	'email' => 'required|valid_email'
	));

if($isValid === true) {
	// Submit Lead
	$Mailchimp = new Mailchimp(MAILCHIMP_API_KEY);
	$Mailchimp_Lists = new Mailchimp_Lists( $Mailchimp );
	$subscriber = $Mailchimp_Lists->subscribe( MAILCHIMP_LIST_ID, array('email' => htmlentities($_POST['email'])));

	if ( ! empty( $subscriber['leid'] ) ) {
	   $result = array(
		'result' => 'success', 
		'msg' => array('<span class="success">¡Felicidades! Revisa tu correo y confirma tu suscripción.</span><script>ga("send", "event", "restaurante", "envio", "success");</script>')
		);

		echo json_encode($result);
	}
	else
	{
	    $result = array(
		'result' => 'error', 
		'msg' => array('<span class="error">No pudimos procesar tu solicitud.</span>')
		);
	}

	

} else {
	$result = array(
		'result' => 'error', 
		'msg' => $isValid
		);

	echo json_encode($result);
}